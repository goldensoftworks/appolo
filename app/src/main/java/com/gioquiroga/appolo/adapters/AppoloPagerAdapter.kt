package com.gioquiroga.appolo.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.gioquiroga.appolo.FavoritesFragment
import com.gioquiroga.appolo.PicturesListFragment


const val FAVORITES_PAGE_INDEX = 0
const val PICTURES_LIST_PAGE_INDEX = 1

class AppoloPagerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    /**
     * Mapping of the ViewPager page indexes to their respective Fragments
     */
    private val tabFragmentsCreators: Map<Int, () -> Fragment> = mapOf(
        FAVORITES_PAGE_INDEX to { FavoritesFragment() },
        PICTURES_LIST_PAGE_INDEX to { PicturesListFragment() }
    )

    override fun getItemCount() = tabFragmentsCreators.size

    override fun createFragment(position: Int): Fragment {
        return tabFragmentsCreators[position]?.invoke() ?: throw IndexOutOfBoundsException()
    }
}