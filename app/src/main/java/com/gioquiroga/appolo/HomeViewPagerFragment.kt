package com.gioquiroga.appolo

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.gioquiroga.appolo.adapters.AppoloPagerAdapter
import com.gioquiroga.appolo.adapters.FAVORITES_PAGE_INDEX
import com.gioquiroga.appolo.adapters.PICTURES_LIST_PAGE_INDEX
import com.gioquiroga.appolo.databinding.FragmentViewPagerBinding
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class HomeViewPagerFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentViewPagerBinding.inflate(inflater, container, false)
        val tabLayout = binding.tabs
        val viewPager = binding.viewPager

        viewPager.adapter = AppoloPagerAdapter(this)

        // Set the icon and text for each tab
        TabLayoutMediator(tabLayout, viewPager) { tab, position ->
            tab.setIcon(getTabIcon(position))
            tab.text = getTabTitle(position)
        }.attach()

        (activity as AppCompatActivity).setSupportActionBar(binding.toolbar)

        return binding.root
    }

    private fun getTabIcon(position: Int): Int {
        return when (position) {
            FAVORITES_PAGE_INDEX -> R.drawable.favorites_tab_selector
            PICTURES_LIST_PAGE_INDEX -> R.drawable.pictures_list_tab_selector
            else -> throw IndexOutOfBoundsException()
        }
    }

    private fun getTabTitle(position: Int): String? {
        return when (position) {
            FAVORITES_PAGE_INDEX -> getString(R.string.favorites_title)
            PICTURES_LIST_PAGE_INDEX -> getString(R.string.pictures_list_title)
            else -> null
        }
    }
}