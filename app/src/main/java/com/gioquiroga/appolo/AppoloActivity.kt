package com.gioquiroga.appolo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil.setContentView
import com.gioquiroga.appolo.databinding.ActivityAppoloBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class AppoloActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView<ActivityAppoloBinding>(this, R.layout.activity_appolo)
    }
}